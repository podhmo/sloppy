sloppy
----------------------------------------

tiny mock view application (pyramid)

how to use
----------------------------------------

sloppy has 2 functions only.

- api (get)
- store (set)

api
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- list
- mock

list -- return registered view 

.. code:: bash

    $ curl http://localhost:6543/api/list
    {"put": [], "get": ["/add", "/hello", "/nested/message"], "delete": [], "post": []}

mock -- use registered view

.. code:: bash

    $ curl http://localhost:6543/api/mock/hello
    {"message": "hello"}

on default implementation, mock view dispatches by request method when access.
so, if you GET access "api/mock/nested/message" then a content of nested/message.get.json is returned.

store
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- mock
- delete

mock -- store data

.. code:: bash

    $ curl http://localhost:6543/store/mock/get/add?ans=30&x=10=y=20
    {"message": "ok"}
    $ curl http://localhost:6543/api/mock/add
    {"ans": "30", "x": "10", "y": "20"}

.. code:: bash

    $ curl http://localhost:6543/api/mock/add
    {"ans": "30", "x": "10", "y": "20"}
    $ curl http://localhost:6543/api/delete/add
    {"message": "ok"}
    $ curl http://localhost:6543/api/mock/add
    {}
