from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include(".")
    config.add_static_view('static', 'static', cache_max_age=3600)
    return config.make_wsgi_app()


def includeme(config):
    from .store.filejson import create_locator_from_assetspec
    config.include(".store")
    store_directory = config.registry.settings["sloppy.store.directory"]
    locator = create_locator_from_assetspec(store_directory)
    config.add_locator(locator)
    config.include(".app")
