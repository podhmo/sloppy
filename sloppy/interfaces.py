# -*- coding:utf-8 -*-
from zope.interface import Interface


class IStoreLocator(Interface):
    def locate(path):
        pass

    def scan():
        pass
