# -*- coding:utf-8 -*-
import logging
logger = logging.getLogger(__name__)
from pyramid.view import view_config
from sloppy.store import get_locator


def request_method_name_only(info, request):
    method = request.matchdict["method"]
    return method.lower() in ["get", "post", "delete", "put"]


@view_config(route_name="store.mock", renderer="json", custom_predicates=(request_method_name_only, ))
def store_mock_view(context, request):
    name = request.matchdict["name"]
    method_name = request.matchdict["method"]

    locator = get_locator(request)
    locator.locate(request, name, request_method=method_name).set(request.params)
    return {"message": "ok"}


@view_config(route_name="store.delete", renderer="json", custom_predicates=(request_method_name_only, ))
def store_delete_view(context, request):
    name = request.matchdict["name"]
    method_name = request.matchdict["method"]

    locator = get_locator(request)
    locator.locate(request, name, request_method=method_name).delete()
    return {"message": "ok"}
