# -*- coding:utf-8 -*-
def includeme(config):
    config.add_route("store.mock", "/store/mock/{method}/{name:.*}")
    config.add_route("store.delete", "/store/delete/{method}/{name:.*}")
    config.scan(".views")
