# -*- coding:utf-8 -*-
import logging
logger = logging.getLogger(__name__)


def includeme(config):
    config.add_route("api.list", "/api/list")
    config.add_route("api.mock", "/api/mock/{name:.*}")
    config.scan(".views")
