# -*- coding:utf-8 -*-
import logging
logger = logging.getLogger(__name__)
from pyramid.view import view_config
from sloppy.store import get_locator


@view_config(route_name="api.list", renderer="json")
def api_list_view(context, request):
    locator = get_locator(request)
    return locator.scan()


@view_config(route_name="api.mock", renderer="json")
def api_mock_view(context, request):
    locator = get_locator(request)
    name = request.matchdict["name"]
    store = locator.locate(request, name)
    return store.get()
