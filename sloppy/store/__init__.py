# -*- coding:utf-8 -*-
import logging
logger = logging.getLogger(__name__)
from ..interfaces import IStoreLocator


def get_locator(request):
    return request.registry.getUtility(IStoreLocator)


def add_locator(config, locator):
    config.registry.registerUtility(locator)


def includeme(config):
    config.add_directive("add_locator", add_locator)
