# -*- coding:utf-8 -*-
import os.path
import json
from pyramid.decorator import reify
from ..interfaces import IStoreLocator
from zope.interface import implementer
from pyramid.path import AssetResolver
import re


@implementer(IStoreLocator)
class FileLocator(object):
    def __init__(self, root, mapper):
        self.root = root
        self.mapper = mapper

    def locate(self, request, path, request_method=None):
        if not request_method:
            request_method = request.method
        request_method = request_method.lower()

        fullpath = "{}.{}.json".format(os.path.join(self.root, path), request_method)
        return self.mapper(fullpath)

    def scan(self):
        result = {"get": [], "delete": [], "post": [], "put": []}
        pairlist_of_name_and_suffix = [(name, ".{}.json".format(name)) for name in result.keys()]
        for r, ds, fs in os.walk(self.root):
            for f in fs:
                for name, suffix in pairlist_of_name_and_suffix:
                    if f.endswith(suffix):
                        path = os.path.join(r, f).replace(self.root, "")
                        path = re.sub("{}$".format(re.escape(suffix)), "", path)
                        result[name].append(path)  # xxx: this is danger
        return result


class JSONFileKeyValueStore(object):
    def __init__(self, filename):
        self.filename = filename

    @reify
    def data(self):
        if os.path.exists(self.filename):
            with open(self.filename) as rf:
                return json.load(rf)
        else:
            return {}

    def get(self):
        return self.data

    def set(self, params):
        self.data.update(params)
        self.save()

    def delete(self):
        if os.path.exists(self.filename):
            os.remove(self.filename)

    def save(self):
        with open(self.filename, "w") as wf:
            wf.write(json.dumps(self.data))


def create_locator_from_assetspec(assetspec, storeclass=JSONFileKeyValueStore):
    path = AssetResolver().resolve(assetspec).abspath()
    if not os.path.exists(path):
        os.makedirs(path)
    return FileLocator(path, storeclass)
