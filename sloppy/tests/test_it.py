# -*- coding:utf-8 -*-
import unittest
import os.path
from testfixtures import compare
tmpdir = os.path.join(os.path.abspath(os.path.dirname((__file__))), "tmpstore")


def setUpModule():
    import shutil
    if os.path.exists(tmpdir):
        shutil.rmtree(tmpdir)


class Tests(unittest.TestCase):
    def _getAUT(self):
        from sloppy import main
        return main

    def _makeApp(self, *args, **kwargs):
        from webtest import TestApp
        settings = {'sloppy.store.directory': tmpdir}
        return TestApp(self._getAUT()({}, **settings))

    def test_it(self):
        app = self._makeApp()

        # first is empty output
        response = app.get('/api/mock/hello')
        compare(response.status_int, 200)
        compare(response.text, u'{}')

        # store content
        response2 = app.post('/store/mock/get/hello', {'message': 'hello'})
        compare(response2.status_int, 200)
        compare(response2.text, u'{"message": "ok"}')

        # after store content output response is changed.
        response3 = app.get('/api/mock/hello')
        compare(response3.status_int, 200)
        compare(response3.text, u'{"message": "hello"}')

        # post method is empty
        response4 = app.post('/api/mock/hello')
        compare(response4.status_int, 200)
        compare(response4.text, u"{}")

    def test_listing(self):
        app = self._makeApp()

        # store content
        response = app.post('/store/mock/get/hello', {'message': 'hello'})
        compare(response.status_int, 200)
        compare(response.text, u'{"message": "ok"}')

        # after store content output response is changed.
        response2 = app.get('/api/list')
        compare(response2.status_int, 200)
        import json
        compare(list(sorted(json.loads(response2.text))),
                list(sorted(json.loads(u'{"get": ["/hello"], "delete": [], "put": [], "post": []}'))))

    def test_deleting(self):
        app = self._makeApp()

        # store content
        response = app.post('/store/mock/get/hello', {'message': 'hello'})
        compare(response.status_int, 200)
        compare(response.text, u'{"message": "ok"}')

        # after store content output response is changed.
        response2 = app.get('/api/list')
        compare(response2.status_int, 200)
        import json
        compare(list(sorted(json.loads(response2.text))),
                list(sorted(json.loads(u'{"get": ["/hello"], "delete": [], "put": [], "post": []}'))))

        # delete content
        response3 = app.post('/store/delete/get/hello')
        compare(response3.status_int, 200)
        compare(response3.text, u'{"message": "ok"}')

        # post method is empty
        response4 = app.get('/api/mock/hello')
        compare(response4.status_int, 200)
        compare(response4.text, u"{}")
