# -*- coding:utf-8 -*-
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response


def index_view(request):
    from js.jquery import jquery_js; jquery_js.need()
    template = """
<html>
<head>
<meta charset="UTF-8"/>
</head>
<body>
hello
</body>
</html>
    """
    return Response(template)

if __name__ == '__main__':
    config = Configurator(settings={"sloppy.store.directory": "sloppy:store"})
    config.include("sloppy")
    config.include("pyramid_fanstatic")
    config.add_route('index', '/')
    config.add_view(index_view, route_name="index")
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever()
